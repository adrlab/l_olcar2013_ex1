function C_sym = C_ballbot(x,u)
%
% Linearized model noise matrix gain.
% dx/xt = f(x,u,e)
% dx/dt = Ax + Bu + Ce
% C     = Jacobian(f,e)
%
% x: State vector
% u: Control input vector
% C_sym: noise matrix gain

C_sym = B_ballbot(x,u)*0.05;

end

