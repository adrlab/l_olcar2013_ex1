function [] = showResults(Model_Param, sim_out)

run parameters/parameter_3d;

addpath('functions')
addpath('libraries')
addpath('simulation')
addpath('simulation/texture')

global fullState tStart vr
tStart = 0;
fullState = odometryIntegration(Model_Param, sim_out);

sim_time = inf;


%% start simulation
vr = vrworld('simulation/ballbot.wrl', 'reuse');
open(vr);
view(vr);
options = simset('SrcWorkspace','current');
sim('visualization',[0 sim_time],options);


