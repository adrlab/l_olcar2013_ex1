function [out] = sampler (time)

global tStart iNow fullState;

 if tStart == 0
     iNow = 2;
     tStart = now;
     stateOut = fullState.x(:,1);
     odometryOut = fullState.odometry(:,1);
     out = [stateOut' odometryOut'];
     return
 end
 
 realT = (now - tStart)* 10^5;
 sleepTime = (fullState.t(iNow) - realT);
 pause(sleepTime);
 
 stateOut = fullState.x(:,iNow);
 odometryOut = fullState.odometry(:,iNow);
 out = [stateOut' odometryOut'];
 
 if iNow+1 > length(fullState.t)
     set_param('visualization', 'SimulationCommand', 'stop')
     return
 else
  iNow = iNow + 1;
 end

end